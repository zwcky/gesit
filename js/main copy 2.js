/**
 * jQuery.browser.mobile (http://detectmobilebrowser.com/)
 *
 * jQuery.browser.mobile will be true if the browser is a mobile device
 *
 **/
(function (a) {
    (jQuery.browser = jQuery.browser || {}).mobile = /(android|bb\d+|meego).+mobile|avantgo|bada\/|android|ipad|playbook|silk|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(a) || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0, 4))
})(navigator.userAgent || navigator.vendor || window.opera);



$(function () {



    //po przeładowaniu strony scroll na górę
    $(window).unload(function () {
        $('body').scrollTop(0);
    });


    //Loadin screen Kiedy wszystkie obrazki są załadowane loading znika
    $('.kurtyny').imagesLoaded()
        .always(function (instance) {

            $('#loading-screen div').animate({
                top: "-30%",
                opacity: 0

            }, 500, 'easeInQuart', function () {
                $('body').css({
                    'overflow-y': 'scroll'
                })
                $('#loading-screen').fadeOut('slow');
            });
        });


    // żeby nie było scrolla na loading screen 
    $('body').css({
        'overflow': 'hidden',
    })



    //dony pasek startu
    $(".start a").click(function (e) {
        // e.preventDefault();
        $(".start").removeClass("active");

        $(".main-nav .our-work a").addClass("active");
        //$(".showcase").fadeOut('fast');

        // $('.fade').cycle('distroy');

    });

    // pokazzuje offset podczas skroliwania
    $(document).scroll(function () {
        // console.log($(this).scrollTop());
        //$('.logo').html($(this).scrollTop());
    });

    if (jQuery.browser.mobile != true) {
        //animacje pod skrollerm
        var s = skrollr.init({
            smoothScrollingDuration: 900,
        });

        skrollr.menu.init(s, {
            animate: true,
            easing: 'sqrt',
            duration: function (currentTop, targetTop) {
                return 400;
            }
        });


        //podczas skrollowania znika start i the studio
        $(document).scroll(function () {
            if ($(this).scrollTop() > 10) {
                $(".start").removeClass("active");
                $(".main-nav .our-work a").addClass("active");
                $(".studio-content-wrapper").removeClass("active");
                $(".main-nav .the-studio a").removeClass("active");
            }
        });
    }



    // dynamiczne wielkości przy resizowaniu
    var the_window = $(window);

    // górne i dolne marginesy

    function homeMargins() {
        var h_margins = (the_window.height() - $('.image-box').height()) / 2;
        console.log(h_margins);
        if (h_margins > 60) {
            console.log(h_margins);
        } else {
            h_margins = 60;
            console.log(h_margins);
        }

        if (jQuery.browser.mobile != true) {
            $('.sidebars').css('top', h_margins);
            $('.text-content').css('height', the_window.height() - h_margins);
            $('#top').css('height', h_margins);
            $('.kurtyna').css('padding-top', h_margins);
            $('.start').css('height', h_margins);
            $('#photo2').attr('data-menu-offset', -h_margins - 55);
            $('body').height($('body').height() - the_window.height() + 100);

        }

        if (jQuery.browser.mobile == true) {
            $('.fade').css('height', ($('.photo-big').eq(2).height()) * 0.91);
            $('.text-content').css('height', ($('.photo-big').eq(2).height()) * 1.2);
        }
        //alert(($('.photo-big').eq(2).height())*0.95));



        $('#loading-screen').css('height', the_window.height());
        $('#lalert').css('height', the_window.height());
        //$('.start').addClass('active');



    };

    homeMargins();

    the_window.resize(function (e) {

        homeMargins();

    });


    // Zmiana języków

    changeLang = function (lang) {

        if (lang == "#pl") {
            $('.lang li a.pl').addClass('active');
            $('.lang li a.en').removeClass('active');
            $('.studio-content-wrapper').load('the_studio_pl.html');
            $('.main-nav').attr('id', 'lang-pl');
            $('.bg img, #kurtyna-2 img, #kurtyna-4 img').attr('src', function (i, e) {
                return e.replace("en-", "pl-");
            })

        } else {
            $('.lang li a.en').addClass('active');
            $('.lang li a.pl').removeClass('active');
            $('.main-nav').attr('id', 'lang-en');
            $('.studio-content-wrapper').load('the_studio_en.html');
            $('.bg img, #kurtyna-2 img, #kurtyna-4 img').attr('src', function (i, e) {
                return e.replace("pl-", "en-");
                console.log('i: ' + i + 'e: ' + e);
            })

        }

    }

    // defaultowy język
    window.location.hash = 'pl';
    changeLang(window.location.hash);

    //zmina jezyków po kliknięciu
    $('.lang li a').click(function (e) {
        e.preventDefault();
        window.location.hash = $(this).attr('href');
        changeLang($(this).attr('href'));
    });


    //dodaje podpisy do zdjęć
    $('.image-box').not('.bg').each(function () {
        $(this).append('<img src="img/photo.png" class="foto-podpis" title="photo by Tomrii">');
    });



    if (jQuery.browser.mobile == true) {

        //dostęp tylko dla wyznaczonego ip
        // $.getJSON("http://smart-ip.net/geoip-json?callback=?",
        //  function (data) {
        // alert(data.host);
        //if (data.host != "89.68.16.249") {
        //  if (data.host != "77.252.247.27") {
        //  $('body').html('<div id="alert"><div>Strona w trakcie budowy<br>na urządzenia mobilne</div></div>')

        //   }
        //});


        // dodaje klasę do body dla mobilnych urządzeń
        $('body').addClass('mobile');

        //podczas skrollowania znika start i the studio
        $(document).scroll(function () {
            if ($(this).scrollTop() > 10) {
                $(".start").removeClass("active");
                $(".main-nav .our-work a").addClass("active");
            }
        });



    }


    //slider

    $('.fade').cycle({
        easing: 'easeInOutQuint'
    });


    //main-menu

    $(".main-nav .our-work a").click(function (e) {
        e.preventDefault();
        $(".start").removeClass("active");
        $(".studio-content-wrapper").removeClass("active");
        $(".main-nav .the-studio a").removeClass("active");
        $(this).addClass("active");
    });


    $(".main-nav .the-studio a").click(function (e) {
        e.preventDefault();
        $(".start").removeClass("active");
        $(".studio-content-wrapper").addClass("active");
        $(".main-nav .our-work a").removeClass("active");
        $(this).addClass("active");

    });



    //menu about studio

    $(document).on('click', '.about-menu li a', function (e) {

        e.preventDefault();

        var clicked_link = $(this).attr("rel");
        console.log(clicked_link);

        $('.about-menu li a').removeClass("active");
        $(this).addClass("active");
        console.log($(clicked_link));

        $(".article-wrapper > article").removeClass("active");
        $(clicked_link).addClass("active");

    });

    // close menu about studio
    $(document).on('click', '.x', function (e) {

        e.preventDefault();
        $(".studio-content-wrapper").removeClass("active");
        $(".main-nav .the-studio a").removeClass("active");
        $(".main-nav .our-work a").addClass("active");
    });




});