var gulp = require('gulp');
var uglify = require('gulp-uglify');
var browserSync = require('browser-sync');
var plumber = require('gulp-plumber');
var gutil = require('gulp-util');
 
var gulp_src = gulp.src;
gulp.src = function() {
  return gulp_src.apply(gulp, arguments)
    .pipe(plumber(function(error) {
      // Output an error message
      gutil.log(gutil.colors.red('Error (' + error.plugin + '): ' + error.message));
      // emit the end event, to properly end the task
      this.emit('end');
    })
  );
};

gulp.task('scripts', function(){
	gulp.src('js/*.js')	
	.pipe(uglify())
	.pipe(gulp.dest('js/min'))
	// .on('error', gutil.log);
});


gulp.task('styles', function() {
    gulp.src('css/*.css')
    .pipe(gulp.dest('css2'));
        
});


gulp.task('watch', function(){
	gulp.watch('js/*.js', ['scripts']);
	gulp.watch('css/*.css', ['styles']);
});

gulp.task('browser-sync', function() {
    browserSync({
      	 proxy: "localhost/Gesit"
    });
});

gulp.task('default', ['scripts','styles','watch','browser-sync']);